package telcel.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import telcel.example.model.User;
import telcel.example.repository.UserRepository;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class UserController {
	@Autowired
	private UserRepository userRepository;


	/**
	 * Crea los 10 usuarios
	 * @return
	 */
	@PostMapping(path = "/crearUsuarios")
	ResponseEntity<String> crearUsuarios() {


		User user1 = new User();
		user1.setNombre("Alberto");
		user1.setApellido("Lopez Vázquez");
		user1.setPuesto("Desarrollo");
		user1.setTiempoExperiencia(5);
		userRepository.save(user1);

		User user2 = new User();
		user2.setNombre("Jorge Luis");
		user2.setApellido("Angeles Diaz");
		user2.setPuesto("Gerente");
		user2.setTiempoExperiencia(3);
		userRepository.save(user2);

		User user3 = new User();
		user3.setNombre("Marcela");
		user3.setApellido("De la Cruz Ríos");
		user3.setPuesto("Analista");
		user3.setTiempoExperiencia(3);
		userRepository.save(user3);

		User user4 = new User();
		user4.setNombre("Marco");
		user4.setApellido("Toledo Duque");
		user4.setPuesto("Analista");
		user4.setTiempoExperiencia(1);
		userRepository.save(user4);

		User user5 = new User();
		user5.setNombre("Maria");
		user5.setApellido("Rojas Davalo");
		user5.setPuesto("Gerente");
		user5.setTiempoExperiencia(5);
		userRepository.save(user5);

		User user6 = new User();
		user6.setNombre("Teresa");
		user6.setApellido("Mejia Tabasco");
		user6.setPuesto("Soporte");
		user6.setTiempoExperiencia(0.416); // 5 meses
		userRepository.save(user6);


		User user7 = new User();
		user7.setNombre("Elvira");
		user7.setApellido("Santos Santos");
		user7.setPuesto("Desarrollo");
		user7.setTiempoExperiencia(2.6); //2.6 años
		userRepository.save(user7);

		User user8 = new User();
		user8.setNombre("Ernesto");
		user8.setApellido("Alvarado Espinoza");
		user8.setPuesto("Soporte");
		user8.setTiempoExperiencia(1);
		userRepository.save(user8);

		User user9 = new User();
		user9.setNombre("Franco");
		user9.setApellido("Arzate Ganoa");
		user9.setPuesto("Desarrollo");
		user9.setTiempoExperiencia(2);
		userRepository.save(user9);

		User user10 = new User();
		user10.setNombre("Laura");
		user10.setApellido("Najera Trullo");
		user10.setPuesto("Analista");
		user10.setTiempoExperiencia(5);
		userRepository.save(user10);




		return new ResponseEntity<>(
				"Usuarios creados..!",
				HttpStatus.OK);
	}

	/**
	 * Dar de alta a un usuario
	 * @param nombre
	 * @param apellido
	 * @param puesto
	 * @param tiempExperiencia
	 * @return
	 */
	@PostMapping(path = "/crearUsuario")
	public @ResponseBody
	ResponseEntity<String> crearUsuario(
			@RequestParam String nombre
			, @RequestParam String apellido,
			@RequestParam String puesto,
			@RequestParam double tiempExperiencia) {


		User newUser = new User();
		newUser.setNombre(nombre);
		newUser.setPuesto(apellido);
		newUser.setPuesto(puesto);
		newUser.setTiempoExperiencia(tiempExperiencia);

		userRepository.save(newUser);
		return new ResponseEntity<>(
				"Usuario creado",
				HttpStatus.OK);
	}

	/**
	 * Obtiene los usuarios con experiencia mayor a 3 años
	 * @return
	 */
	@GetMapping(path = "/getUserWithExperience")
	ResponseEntity<List<User>> getAllUsers() {

		List<User> users = userRepository.findAllUsersWithExperienceGreaterThanThreeYears();
		return new ResponseEntity<>(
				users,
				HttpStatus.OK);
	}

	/**
	 * Actualiza los usuarios con tiempo de experiencia mayor a 5 años a puesto de cordinador
	 * @return
	 */
	@PutMapping(path = "/udpateUsersToCordinador")
	ResponseEntity<String> updateUsersToCordinador() {


		int yearsOfExperience = 5;
		userRepository.updateUsersToCordinador(yearsOfExperience);

		return new ResponseEntity<>(
				"Usuarios actualizados..!",
				HttpStatus.OK);
	}

	/**
	 * Elimina usuarios que tengan menos de 1 año de experiencia y que tengan puesto de soporte
	 * @return
	 */
	@DeleteMapping(path = "/delteUsers")
	ResponseEntity<String> deleteUsers() {

		int yearsOfExperience = 1;
		userRepository.deleteUsers(yearsOfExperience);
		return new ResponseEntity<>(
				"Usuarios eleminados",
				HttpStatus.OK);
	}
}

package telcel.example.repository;

		import org.springframework.data.jpa.repository.Modifying;
		import org.springframework.data.jpa.repository.Query;
		import org.springframework.data.repository.CrudRepository;
		import org.springframework.data.repository.query.Param;
		import telcel.example.model.User;

		import java.util.List;


public interface UserRepository extends CrudRepository<User, Integer> {

	/**
	 * Regresa los usuarios que tengan un tiempo de experienca mayor a 3 años
	 * @return list of user
	 */
	@Query("SELECT u.id, u.apellidos,u.puesto FROM User u WHERE u.tiempoExperiencia > 3")
	List<User> findAllUsersWithExperienceGreaterThanThreeYears();

	/**
	 * Actualiza los usuarios que tengan tiempo de experiencia mayor a  5 años a puesto de cordinador
	 * @param tiempoExperiencia
	 * @return numbero de registros actualizados
	 */
	@Modifying
	@Query("update User u set u.puesto = 'cordinador' where u.tiempoExperiencia > :tiempoExperiencia")
	int updateUsersToCordinador(@Param("tiempoExperiencia") Integer tiempoExperiencia);

	/**
	 * Elimina los usuarios que tengan puesto de soport y que su tiempo de experiencia se menor 1 año
	 * @param tiempoExperiencia
	 */
	@Modifying
	@Query("delete from User u where u.puesto='Soporte' and u.tiempoExperiencia < :tiempoExperiencia")
	void deleteUsers(@Param("tiempoExperiencia") Integer tiempoExperiencia);

}
